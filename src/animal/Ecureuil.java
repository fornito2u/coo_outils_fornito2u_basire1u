package animal;

public class Ecureuil implements Animal 
{
	private int noisette;
	private int pointDeVie;
	
	
	public Ecureuil(){
		this.noisette = 0;
		this.pointDeVie = 5;
	}
	
	public Ecureuil(int pointDeVie){
		this.noisette = 0;
		if (pointDeVie < 0)
			this.pointDeVie = 0;
		else
			this.pointDeVie = pointDeVie;
	}
	
	
	public boolean etreMort() 
	{
		if (pointDeVie <= 0)
			return true;
		else
			return false;
	}

	@Override
	public boolean passerUnjour() 
	{
		if (etreMort())
			return false;
		
		if (this.noisette > 0)
		{
			this.noisette -= 1;
			if (this.noisette > 0)
			{
				this.noisette -= 1;
			}
		}
		else
		{
			this.pointDeVie -= 2;
			if (etreMort())
				return false;
		}
		
		return true;
			
	}

	@Override
	public void stockerNourriture(int nourriture) 
	{
		if (!etreMort()){
			this.noisette += nourriture;
		}	
	}

	@Override
	public int getPv() 
	{
		return this.pointDeVie;
	}

	@Override
	public int getStockNourriture() 
	{
		return this.noisette;
	}

	@Override
	public String toString() {
		return "Ecureuil - pv:"+this.pointDeVie+" noisettes:"+this.noisette;
	}

}
