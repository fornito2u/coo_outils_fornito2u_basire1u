package animal;

public class Loup implements Animal
{
	private int viande, pointDeVie;
	
	public Loup()
	{
		this.pointDeVie=15;
		this.viande=0;
	}
	
	public Loup(int pdv)
	{
		this.viande=0;
		if(pdv>=0)
		{
			this.pointDeVie=pdv;
		}
		else
			this.pointDeVie=0;
	}
	
	public boolean etreMort() 
	{
		boolean b=true;
		if(this.getPv()-4>0)
		{
			b=false;
		}
		return b;
	}
	
	
	public boolean passerUnjour()
	{
		boolean b=false;
		if(!etreMort())
		{
			if(this.getStockNourriture()>0)
			{
				this.viande-=1;
				this.viande-=this.viande/2;
			}
			else
				this.pointDeVie-=4;
			b=true;
		}
		return b;
	}
	
	public void stockerNourriture(int nourriture)
	{
		if(!etreMort())
		{
			this.viande+=nourriture;
		}
	}
	
	
	public int getPv()
	{
		return this.pointDeVie;
	}
	
	public int getStockNourriture()
	{
		return this.viande;
	}
	
	public String toString()
	{
		return("Loup - pv:"+this.getPv()+" viande:"+this.getStockNourriture());
	}
}
