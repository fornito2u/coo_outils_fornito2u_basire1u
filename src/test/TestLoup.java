package test;
import static org.junit.Assert.*;

import org.junit.Test;

import animal.Loup;


public class TestLoup {
	
	@Test
	public void testConstructeur() {
		Loup fenrir = new Loup();
		
		assertEquals("Le loup devrait avoir 15 Pv",15,fenrir.getPv());
		assertEquals("Le loup devrait avoir 0 Viande",0,fenrir.getStockNourriture());
	}
	
	
	@Test
	public void testConstructeurParam() {
		Loup fenrir = new Loup(4);
		Loup L = new Loup(0);
		
		
		assertEquals("Le loup devrait avoir 4 Pv",4,fenrir.getPv());
		assertEquals("Le loup devrait avoir 0 Viande",0,fenrir.getStockNourriture());
		
		assertEquals("Le loup numero 2 devrait avoir 0 Pv",0,L.getPv());
		
	}
	@Test
	public void testConstructeurParamPVNegatif() 
	{
		Loup loupMortVivant = new Loup(-5);
		
		assertEquals("Le loup numero 3 devrait avoir 0 Pv",0,loupMortVivant.getPv());
	}
	
	@Test
	public void testStockerNourritureMort()
	{
		Loup fenrir = new Loup(0);
		fenrir.stockerNourriture(6);
		
		assertEquals("Le loup devrait avoir 0 Steak en stock",0,fenrir.getStockNourriture());
	}
	
	@Test
	public void testStockerNourritureVivant()
	{
		Loup fenrir = new Loup();
		fenrir.stockerNourriture(6);
		
		assertEquals("Le loup devrait avoir 6 Steak en stock",6,fenrir.getStockNourriture());
	}
	
	@Test
	public void testPasserUnJourVivantAvecNouriture()
	{
		Loup fenrir = new Loup(5);
		fenrir.stockerNourriture(8);
		assertEquals("La methode devrait retourner true",true,fenrir.passerUnjour());
		assertEquals("Le loup devrait avoir 4 viande apr�s une journer",4,fenrir.getStockNourriture());
		assertEquals("Le loup devrait avoir 5 Pv",5,fenrir.getPv());
		
		
		Loup l = new Loup(5);
		l.stockerNourriture(1);
		assertEquals("La methode devrait retourner true",true,l.passerUnjour());
		assertEquals("Le loup devrait avoir 0 viande apr�s une journer",0,l.getStockNourriture());
		assertEquals("Le loup devrait avoir 5 Pv",5,l.getPv());
	}
	
	@Test
	public void testPasserUnJourVivantSansNourriture()
	{
		Loup fenrir = new Loup(5);
		assertEquals("La methode devrait retourner true",true,fenrir.passerUnjour());
		assertEquals("Le loup devrait avoir 0 viande apr�s une journer",0,fenrir.getStockNourriture());
		assertEquals("Le loup devrait avoir 1 Pv",1,fenrir.getPv());	
	}
	
	@Test
	public void testPasserUnJourMort()
	{
		Loup fenrir = new Loup(0);
		fenrir.passerUnjour();
		assertEquals("La methode devrait retourner false",false,fenrir.passerUnjour());
		assertEquals("Le loup devrait avoir 0 viande apr�s une journer",0,fenrir.getStockNourriture());
		assertEquals("Le loup devrait avoir 0 Pv",0,fenrir.getPv());	
	}

}




















