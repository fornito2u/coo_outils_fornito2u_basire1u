package test;

import static org.junit.Assert.*;

import org.junit.Test;

import animal.Ecureuil;

public class TestEcureuil 
{
	@Test
	public void testConstructeurSansParametre()
	{
		Ecureuil e=new Ecureuil();
		assertEquals("L'ecureuil devrait avoir 5 point de vie", 5, e.getPv());	
		assertEquals("L'ecureuil devrait avoir 0 viande", 0, e.getStockNourriture());
	}
	
	@Test
	public void testConstructeurAvecParametrePdvPositif()
	{
		Ecureuil e=new Ecureuil(9);
		assertEquals("L'ecureuil devrait avoir 5 point de vie", 9, e.getPv());	
		assertEquals("L'ecureuil devrait avoir 0 viande", 0, e.getStockNourriture());
	}
	
	@Test
	public void testConstructeurAvecParametrePdvNegatif()
	{
		Ecureuil e=new Ecureuil(-5);
		assertEquals("L'ecureuil devrait avoir 0 viande", 0, e.getStockNourriture());
		assertEquals("L'ecureuil devrait avoir 0 point de vie", 0, e.getPv());
	}
	
	@Test
	public void testMethodeEtreMort()
	{
		Ecureuil e=new Ecureuil(0);
		assertEquals("L'ecureuil devrait etre mort", true, e.etreMort());
	}	
	
	@Test
	public void testMethodeStockerNourritureEcureuilVivant()
	{
		Ecureuil e=new Ecureuil();
		e.stockerNourriture(2);
		assertEquals("La nourriture stocker devrait valoir 2", 2, e.getStockNourriture());
	}	
	
	@Test
	public void testMethodeStockerNourritureEcureuilMort()
	{
		Ecureuil e=new Ecureuil(0);
		e.stockerNourriture(2);
		assertEquals("La nourriture stocker devrait valoir 0", 0, e.getStockNourriture());
	}	
	
	@Test
	public void testMethodePasserUnJour()
	{
		Ecureuil e=new Ecureuil();
		e.stockerNourriture(3);
		e.passerUnjour();
		assertEquals("Il devrait avoir 1 seul noisette", 1, e.getStockNourriture());
		e.passerUnjour();
		assertEquals("Les pv doivent etre � 5", 5, e.getPv());
		e.passerUnjour();
		assertEquals("Les pv doivent etre � 3", 3, e.getPv());
	}	
}